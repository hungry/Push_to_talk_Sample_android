package mars.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SocketActivity extends Activity {
    /** Called when the activity is first created. */
	private Button startButton = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        startButton = (Button)findViewById(R.id.startListener);
        startButton.setOnClickListener(new StartSocketListener());
    }
    
    class StartSocketListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			new ServerThread().start();
		}
    	
    }
    class ServerThread extends Thread{
    	public void run(){
    		try {
                //1.建立一个服务器Socket(ServerSocket)绑定指定端口
                ServerSocket serverSocket=new ServerSocket(8800);
                //2.使用accept()方法阻止等待监听，获得新连接
                Socket socket=serverSocket.accept();
                //3.获得输入流
                InputStream is=socket.getInputStream();
                BufferedReader br=new BufferedReader(new InputStreamReader(is));
                //获得输出流
                OutputStream os=socket.getOutputStream();
                PrintWriter pw=new PrintWriter(os);
                //4.读取用户输入信息
                String info=null;
                while(!((info=br.readLine())==null)){
                    System.out.println("我是服务器，用户信息为："+info);
                }
                //给客户一个响应
                
                String reply="welcome";
                pw.write(reply);
                pw.flush();
                //5.关闭资源
                pw.close();
                os.close();
                br.close();
                is.close();
                socket.close();
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }    
    		
    		
    		
    		
    		/*//声明一个ServerSocket对象
    		ServerSocket serverSocket = null;
    		try {
    			//创建一个ServerSocket对象，并让这个Socket在4567端口监听
				serverSocket = new ServerSocket(4567);
				//调用ServerSocket的accept()方法，接受客户端所发送的请求
				Socket socket = serverSocket.accept();
				//从Socket当中得到InputStream对象
				InputStream inputStream = socket.getInputStream();
				byte buffer [] = new byte[1024*4];
				int temp = 0;
				//从InputStream当中读取客户端所发送的数据
				while((temp = inputStream.read(buffer)) != -1){
					System.out.println(new String(buffer,0,temp));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				try {
					serverSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
		
    	}
    }
}