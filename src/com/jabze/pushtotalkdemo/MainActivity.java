package com.jabze.pushtotalkdemo;

import java.io.IOException;

import com.jabze.utils.HttpDownloader;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    private Button downloadMp3Button;
	private Button recorderButton;
	private MediaRecorder theRecord=null;
	private int xxx=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        downloadMp3Button = (Button)findViewById(R.id.downloadAudio);
        downloadMp3Button.setOnClickListener(new DownloadMp3Listener());
        recorderButton = (Button)findViewById(R.id.record);
        recorderButton.setOnClickListener(new RecorderListener());
    }
    

   class DownloadMp3Listener implements OnClickListener{

       @Override
		public void onClick(View v) {
			new DownLoadThread().start();
		}
    	
    }
    class RecorderListener implements OnClickListener{
    	
    	@Override
		public void onClick(View v) {
			
    		System.out.print("RecorderListener");
			
			if (theRecord!=null)//如果已经开始录音，则结束录音并播放
			{
				recorderButton.setText("播放中");
				theRecord.stop();  
				theRecord.release();  
				theRecord = null;
				MediaPlayer	mPlayer = new MediaPlayer();  
		        try {  
		        //设置要播放的文件  
		            mPlayer.setDataSource(Environment.getExternalStorageDirectory() + "/test/test.amr"); 
		            mPlayer.prepare();  
		        //播放之  
		            mPlayer.start();  
		        } catch (Exception e) {  
		            Log.e(e.toString(),e.toString());  
		        }  
			}
			else//如果没有在录音，则打开录音
			{
			recorderButton.setText("录音中");
			theRecord=new MediaRecorder();

			//设置音源为麦克风 
			theRecord.setAudioSource(MediaRecorder.AudioSource.MIC); 
	        //设置封装格式  
			theRecord.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB); 
			//设置编码格式 
			theRecord.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			String mp3Path=Environment.getExternalStorageDirectory() + "/test/test.amr";
			theRecord.setOutputFile(mp3Path);  
	        try {
				theRecord.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
	  
	        theRecord.start(); 
	        //Thread theThread=new Thread();
	        
	        
			}
    	}

    }
    class DownLoadThread extends Thread{
    	@Override
		public void run(){
    		// TODO Auto-generated method stub
			HttpDownloader httpDownloader = new HttpDownloader();
			int result = httpDownloader.downFile("http://download.coolfay.com/audio/TestAudio.mp3", "Test/", "Test.mp3");
			if (result==0){
				System.out.println("下载成功！");  
				String mp3Path=Environment.getExternalStorageDirectory() + "/Test/Test.mp3";
				Uri mp3uri = Uri.parse(mp3Path);
				MediaPlayer m= MediaPlayer.create(getApplicationContext(), mp3uri);
				m.start();
		   }else{
			   System.out.println("下载失败！");  
		   }
    	}
    }
    
}