package com.jabze.pushtotalkdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;



public class sendMessageActivity extends Activity {
	TextView sendTextView=null;
    TextView receiveTextView=null;
    Button sendButton=null;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_message);
		sendTextView=(TextView)findViewById(R.id.sendmegtxt);
		receiveTextView=(TextView)findViewById(R.id.datetimetxt);
		sendButton=(Button)findViewById(R.id.startListener);
		sendButton.setOnClickListener(new SendButton());
	}
	class SendButton implements OnClickListener{
		@Override
		public void onClick(View v){
			 try {
				    System.out.print("客户端程序启动。。。");
		            //1.建立客户端socket连接，指定服务器位置及端口
		            Socket socket =new Socket("192.168.1.200",8800);
		            //2.得到socket读写流
		            OutputStream os=socket.getOutputStream();
		            PrintWriter pw=new PrintWriter(os);
		            //输入流
		            InputStream is=socket.getInputStream();
		            BufferedReader br=new BufferedReader(new InputStreamReader(is));
		            //3.利用流按照一定的操作，对socket进行读写操作
		            String sendMessage=sendTextView.getText().toString();
		            pw.write(sendMessage);
		            pw.flush();
		            socket.shutdownOutput();
		            //接收服务器的相应
		            String reply=null;
		            while(!((reply=br.readLine())==null)){
		                System.out.println("接收服务器的信息："+reply);
		                receiveTextView.setText(reply);
		            }
		            //4.关闭资源
		            br.close();
		            is.close();
		            pw.close();
		            os.close();
		            socket.close();
		        } catch (UnknownHostException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		}
	/*	public void onClick(View v) {
			// TODO Auto-generated method stub
			try{
				Socket socket=new Socket("192.168.1.104",1234);
				String message=sendTextView.getText().toString();
				InputStream inputStream=new FileInputStream("f://file/words.txt");
				//InputStream inputStream=new bufferedread();
			    OutputStream outputStream=socket.getOutputStream();
				byte buffer [] =new byte[4*1024];
				int temp=0;
				while((temp=inputStream.read(buffer))!=-1){
					outputStream.write(buffer,0,temp);
				}
				outputStream.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
		}*/
		
	}
  
}
